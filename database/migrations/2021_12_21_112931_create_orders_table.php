<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->timestamps();


            $table->decimal('total_payment_due', 5, 2);

            // POSSIBLE SYNTAX
            // foreign key to the user related to the order
            // first, define the column name (should be snake case name_of_the_class_id)
            //     $table->unsignedBigInteger('user_id');

            // Define the constraint. "user_id" is a foreign key to the "id" column of the table "users"
            // which is why it it spelled in plural.
            //     $table->foreign('user_id')->references('id')->on('users');

            // EASY SYNTAX
            $table->foreignId('user_id')->constrained();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
