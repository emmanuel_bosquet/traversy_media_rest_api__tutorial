## Authentication with Sanctum

### Setup

Install Sanctum with Composer.

    composer require laravel/sanctum

Publish the sanctum configuration and migration files. I don't know what it means but it seems important.

    php artisan vendor:publish --provider="Laravel\Sanctum\SanctumServiceProvider"

    Copied Directory [/vendor/laravel/sanctum/database/migrations] To [/database/migrations]
    Publishing complete.

But this directory might already be present and might not need copying. It contains:

```php
// migration "create_personal_access_tokens_table"
public function up()
{
    Schema::create('personal_access_tokens', function (Blueprint $table) {
        $table->id();
        $table->morphs('tokenable');
        $table->string('name');
        $table->string('token', 64)->unique();
        $table->text('abilities')->nullable();
        $table->timestamp('last_used_at')->nullable();
        $table->timestamps();
    });
}
```

Perform this if necessary:

    php artisan migrate

Add this to `app/Http/Kernel.php`:

```php
protected $middlewareGroups = [
    // stuff
    'api' => [
        \Laravel\Sanctum\Http\Middleware\EnsureFrontendRequestsAreStateful::class,
        'throttle:api',
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
    ],
];
```

or uncomment the line if it's there already.

### Adapt the user model

Add this to the user model (it may be there already):

```php
use Laravel\Sanctum\HasApiTokens;
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    // ...
}
```

The `HasApiTokens` trait has a protected `accessToken` property, and methods to:

-   create a token with certain abilities, for a user,
-   get all tokens for a given user,
-   get the current token
-   check the scope of a token
-   give the token to the class that satisfies the trait (ex. set a user's token)

### Protect routes with a guard

A middleware route for `/user` was already set in the `api.php` file, let's make sure it says `auth:api`.

```php
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
```

And protect routes with it:

```php
// public routes
Route::get('/products', [ProductController::class, 'index']);
Route::get('/products/{id}', [ProductController::class, 'show']);
Route::get('/products/search/{name}', [ProductController::class, 'search']);


// protected routes
Route::group(['middleware' => ['auth:sanctum']],  function () {
    Route::post('/products', [ProductController::class, 'store']);
    Route::put('/products/{id}', [ProductController::class, 'update']);
    Route::delete('/products/{id}', [ProductController::class, 'destroy']);
});
```

Now we receive a nice

```json
{ "message": "Unauthenticated" }
```

When trying to request those protected routes.

### Register a user

Make an Auth controller:

    php artisan make:controller AuthController

In the new file, import the user model, http request and response, and the `bcrypt` hash.

    use App\Models\User;
    use Illuminate\Http\Request;
    use Illuminate\Http\Response;
    use Illuminate\Support\Facades\Hash;

Make a `register` method in the AuthController, with:

-   validation logic
-   user creation
-   token creation
-   201 response

```php
public function register(Request $request)
{
    $fields = $request->validate([
        'name' => 'required|string',
        // the email should be unique in the users table on the email field
        'email' => 'required|string|unique:users,email',
        // the password should be sent twice, as "password" and "password_confirmation"
        'password' => 'required|string|confirmed'
    ]);
    $user = User::create([
        'name' => $fields['name'],
        'email' => $fields['email'],
        'password' => bcrypt($fields['password'])
    ]);
    // createToken is a method of HasApiToken, which User satisfies
    $token = $user->createToken('myapptoken')->plainTextToken;
    $response = [
        'user' => $user,
        'token' => $token
    ];
    return response($response, 201);
}
```

Add the adequate route, a public one:

```php
Route::post('/register', [AuthController::class, 'register']);
```

Sending this:

    POST /api/register HTTP/1.1
    Host: localhost:8000
    Accept: application/json
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 94

    name=John Doe&email=john.doe@mail.com&password=password1234&password_confirmation=password1234

Returns this:

```json
    "user": {
        "name": "John Doe",
        "email": "john.doe@mail.com",
        "updated_at": "2021-12-20T21:11:05.000000Z",
        "created_at": "2021-12-20T21:11:05.000000Z",
        "id": 1
    },
    "token": "1|FfcjNRm386XG2a9H8v8TVA513fGlLKhB2MV9wi0m"
}
```

We can use this token like so:

    POST /api/products HTTP/1.1
    Host: localhost:8000
    Accept: application/json
    Authorization: Bearer 1|FfcjNRm386XG2a9H8v8TVA513fGlLKhB2MV9wi0m
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 46

    name=Product Two&slug=product-two&price=299.99

And it works!

### Log a user out

Add a `logout` method to the Auth Controller, to delete the token from the database.

```php
public function logout(Request $request)
{
    auth()->user()->tokens()->delete();

    return [
        'message' => "Logged out"
    ];
}
```

And a **protected** route:

```php
Route::post('/logout', [AuthController::class, 'logout']);
```

## Log a user in

Create a `login` method to the AuthController (logic akin to `register`):

```php
public function login(Request $request)
{
    $fields = $request->validate([
        'email' => 'required|string',
        'password' => 'required|string'
    ]);

    // find the user who matches first the given email
    // emails are unique so this is lazy syntax
    $user = User::where('email', $fields['email'])->first();
    if (!$user) {
        return response([
            'message' => 'No user is registered with that email'
        ], 401);
    }

    if (
        // use bcrypt to...
        !Hash::check(
            // ...hash the provided password
            $fields['password'],
            // ...compare it with the stored hash
            $user->password
        )
    ) {
        return response([
            'message' => 'Wrong password'
        ], 401);
    }

    $token = $user->createToken('myapptoken')->plainTextToken;

    $response = [
        'user' => $user,
        'token' => $token
    ];

    return response($response, 201);
}
```

Make a **public** route:

    Route::post('/login', [AuthController::class, 'login']);
