# Order: an excuse to create relations

How to create relationships between classes? How to persist them?
Let's create add an Order model, where a user orders several Products. This necessitates all kinds of relations.

```
+-----------------------+     +------------------------------+    +----------------------+
|         Product       |     |          Order               |    |   User               |
+-----------------------+     +------------------------------+    +----------------------+
| -name:        string  |     | -customer:        User       |    | -name:     string    |
| -slug:        string  |     | -products:        Products[] |    | -password: string    |
| -description: string  |     | -totalPaymentDue: int        |    |                      |
| -price:       int     |     |                              |    |                      |
|                       |     |                              |    |                      |
+------------------+----+     +------------------------------+    +---+------------------+
                   |               ^              ^                   |
                   |               |              |                   |
                   +---------------+              +-------------------+
                     included several times           included once
```

There are two relationships here:

-   User <=> Order : one-to-many. A user may pass several orders
-   Order <=> Product : many-to-many. An order has several items, an item may be ordered several times over.

## Make the order's model and controller

    php artisan make:controller OrderController --api

    php artisan make:model Order --migration

Now the one million dollar question: where do we define the relationship? In the Model? In the migration?

The doc says:

> Eloquent relationships are defined as methods on your Eloquent model classes.

Cool. (The relationships serve as query builders, which is even cooler.)

## User-Order: One to Many

A user passes several orders:

```php
// Models/User.php

public function orders()
{
    return $this->hasMany(Order::class);
}
```

An order belongs to a user

```php
// app/Models/Order.php

public function user()
{
    return $this->belongsTo(User::class);
}
```

In the `create_orders_table` migration, write the foreign key to the `users` table:

```php
// migrations/create_orders_table.php
$table->decimal('total_payment_due', 5, 2);

$table->foreignId('user_id')->constrained();
```

This syntax is rather handy, isn't it?
The `user_id` format, with singular table name, underscore and `id`, is standard.

Then migrate.

    php artisan migrate

## Order-Product: many-to-many

We have to create _relationship table_ called `order_product`.
Each row will contain two foreign keys: one to an `id` of `orders`, one to an `id` of `products`.

| order_id | product_id |
| -------- | ---------- |
| 1        | 3          |
| 1        | 2          |
| 2        | 5          |
| 2        | 2          |

    php artisan make:migration create_pivot_table_order_product

-   Correct its name to `order_product`.
-   add a foreign key to `order_id`
-   add a foreign key to `product_id`
-   adding a `cascade constraint` seems undesirable here

```php
// migrations/create_pivot_table_order_product.php

public function up()
{
    Schema::create('order_product', function (Blueprint $table) {
        $table->id();
        $table->timestamps();
        $table->foreignId('order_id')->constrained();
        $table->foreignId('product_id')->constrained();
    });
}
```

Write methods in the controllers, both with `belongsToMany`:

```php
// Models/Order.php
public function products()
{
    return $this->belongsToMany(Product::class);
}

// Models/Product.php
public function orders()
{
    return $this->belongsToMany(Order::class);
}
```

## Relationships are done, now let's make the insertion

The `index` and `show` methods of `OrderController` are rather obvious, let's get interested in the insertion of new orders.

Let's make a Route to post new orders:

```php
Route::post('/orders', [OrderController::class, 'store']);
```

Now let's focus on the `store` method:

```php
// Controllers/OrderController.php

public function store(Request $request)
{
    // wonderful thing to be written in here
}
```

Retrieve the user from the authentication environment.

```php
$user = auth()->user();
```

Validate the request parameters, it should be in the form `["1", "2", "5"]`.

```php
$fields = $request->validate([
    'products' => 'required|array',
    'products.*' => 'required|string'
]);
$products = $fields['products'];
```

Iterate over the product ids, and for each one:

-   retrieve the product in the database (if not present, return an error)
-   add its price to the total amount due (we're here to make money)

```php
for ($i = 0; $i < count($products); $i++) {
    $product = Product::find($products[$i]);

    if ($product == null) {
        return response("Product ${products[id]} is gone apparently :-(", 410);
    }

    $totalPaymentDue += $product->price;
}
```

Create an order from the user and from the total money to pay.
The `create` function is the eloquent way to send an INSERT request,
and returns a ready-made instance `Order` that we will return to the client.

```php
$order = Order::create([
    'user_id' => $user->id,
    'total_payment_due' => $totalPaymentDue
]);
```

Because `Order` has a `belongsTo` relationship with `User`,
the user id is written as a foreign key in the `orders` table. Pretty cool.

Before we return `$order`, some black magic.
What is the eloquent way to say:
"please insert as many rows in the `order_product` pivot table as they are products to link to this order"?

```php
$order->products()->attach($products);
```

Wow. The Eloquent guys have been busy coding this stuff.

Now return the order to the client and we're done.

```php
return $order;
```


### Retrieving the products is so easy!

```php
$orders = Order::all();
foreach ($orders as $order) {
    $order->products;
}
return $orders;
```

And voilà!