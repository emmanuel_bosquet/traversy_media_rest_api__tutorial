# Follow the tutorial

## Setup sqlite

In the env:

    DB_CONNECTION=sqlite
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABESE=database/database.sqlite

Create database file

    touch database/database.sqlite

## API routes

If, in `routes/api.php`, we define this:

```php
Route::get('/products', function () {
    // whatever
});
```

We actually have to query `domain_url/api/products`.

## Create a model

    php artisan make:model Product --migration

This creates:

    app/Models/Product.php
    database/migrations/2021_12_19_133944_create_products_table.php

Weirdly enough, we define the properties of Product **in the migration file**, not in the model.

```php
public function up()
{
    Schema::create('products', function (Blueprint $table) {
        $table->id();
        $table->string('name');
        $table->string('slug');
        $table->string('description')->nullable();
        $table->decimal('price', 5, 2);
        $table->timestamps();
    });
}
```

apply with

    php artisan migrate

This migrates all default laravel migrations too.

    Migration table created successfully.
    Migrating: 2014_10_12_000000_create_users_table
    Migrated:  2014_10_12_000000_create_users_table (163.64ms)
    Migrating: 2014_10_12_100000_create_password_resets_table
    Migrated:  2014_10_12_100000_create_password_resets_table (189.31ms)
    Migrating: 2019_08_19_000000_create_failed_jobs_table
    Migrated:  2019_08_19_000000_create_failed_jobs_table (190.36ms)
    Migrating: 2019_12_14_000001_create_personal_access_tokens_table
    Migrated:  2019_12_14_000001_create_personal_access_tokens_table (333.47ms)
    Migrating: 2021_12_19_133944_create_products_table
    Migrated:  2021_12_19_133944_create_products_table (100.76ms)

### Post Products

We can try this:

```php
// routes/api.php
use App\Models\Product;

Route::post('/products', function () {
    return Product::create([
        'name' => 'Product One',
        'slug' => 'product-1',
        'description' => 'this is product 1',
        'price' => '99.99'
    ]);
});
```

But we have to change the model (at least!) with fillable properties:

```php
// app/models/Product.php
protected $fillable = [
    'name',
    'slug',
    'description',
    'price'
];
```

But filling a table within the routes file is bad. We need a controller.

## Make a controller

    php artisan make:controller ProductController --api

This creates:

    app/Http/Controllers/ProductController.php

Import the product model in there:

```php
use App\Models\Product;
```

### Get requests

The index method of the controller will contain the return-all logic.

```php
// app/Http/Controllers/ProductController.php
public function index()
{
    return Product::all();
}
```

The route becomes:

```php
use App\Http\Controllers\ProductController;

Route::get('/products', [ProductController::class, 'index']);
```

### Post requests

The `store` method stores the data before persisting it.

```php
// app/Http/Controllers/ProductController.php
public function store(Request $request)
{
    return Product::create($request->all());
}
```

The route becomes:

```php
Route::post('/products', [ProductController::class, 'store']);
```

IMPORTANT: add the `Accept` header to your requests, with the value `application/json`.  
As for the parameters, pass them as `x-www-form-urlencoded`.

For further logical stuff like validation, look at the controller code itself.

### Fetch one product with an id

Let's refactor the routes:

```php
// routes/api.php
Route::resource('products' , ProductController::class);
```

This allows routes for CRUD operations :-) List them with

    php artisan route:list

| Domain | Method | URI                    | Name                        | Action                                         | Middleware                                                 |     |
| ------ | ------ | ---------------------- | --------------------------- | ---------------------------------------------- | ---------------------------------------------------------- | --- |
|        | GET    | HEAD                   | /                           |                                                | Closure                                                    | web |
|        | GET    | HEAD                   | api/products                | products.index                                 | App\Http\Controllers\ProductController@index               | api |
|        | POST   | api/products           | products.store              | App\Http\Controllers\ProductController@store   | api                                                        |
|        | GET    | HEAD                   | api/products/create         | products.create                                | App\Http\Controllers\ProductController@create              | api |
|        | GET    | HEAD                   | api/products/{product}      | products.show                                  | App\Http\Controllers\ProductController@show                | api |
|        | PUT    | PATCH                  | api/products/{product}      | products.update                                | App\Http\Controllers\ProductController@update              | api |
|        | DELETE | api/products/{product} | products.destroy            | App\Http\Controllers\ProductController@destroy | api                                                        |
|        | GET    | HEAD                   | api/products/{product}/edit | products.edit                                  | App\Http\Controllers\ProductController@edit                | api |
|        | GET    | HEAD                   | api/user                    |                                                | Closure                                                    | api |
|        |        |                        |                             |                                                | App\Http\Middleware\Authenticate:sanctum                   |
|        | GET    | HEAD                   | sanctum/csrf-cookie         |                                                | Laravel\Sanctum\Http\Controllers\CsrfCookieController@show | web |

### Update a product

```php
public function update(Request $request, $id)
{
    $product = Product::find($id);
    $product->update($request->all());
    return $product;
}
```

### Delete

```php
public function destroy($id)
{
    return Product::destroy($id);
}
```

### Search

```php
public function search($name)
{
    return Product::where('name', 'like', '%'.$name.'%')->get();
}
```

And add a route:

```php
Route::get('/products/search/{name}', [ProductController::class, 'search']);
```
