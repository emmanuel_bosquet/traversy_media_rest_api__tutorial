# Start on a new machine

Recreate `.env` from the example

    cp .env.example .env

Fill it with:

    DB_CONNECTION=sqlite
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABESE=database/database.sqlite

Install dependencies with composer:

    composer i

Generate an app key:

    php artisan key:generate

Create a database (sqlite):

    touch database/database.sqlite

Migrate:

    php artisan migrate

Start laravel

    php artisan serve

Check that it works

    curl http://127.0.0.1/api/products
