<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    // this is facultative
    // protected $table = 'my_product';

    protected $fillable = [
        'name',
        'slug',
        'description',
        'price'
    ];

    /**
     * Get all orders passed by the user
     * This is matched by a belongToMany() on the Product model.
     */
    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }
}
