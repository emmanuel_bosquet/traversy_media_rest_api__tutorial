<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Will become the "orders" table, with snake_case and plural
 */
class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        // is this one usefull?
        // 'products',
        'total_payment_due'
    ];

    /**
     * Get the user that passed the order
     * This is matched by a hasMany() on the User model.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the products of the order
     * This is matched by a belongToMany() on the Product model.
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
