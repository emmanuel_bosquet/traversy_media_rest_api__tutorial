<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $fields = $request->validate([
            'name' => 'required|string',
            // the email should be unique in the users table on the email field
            'email' => 'required|string|unique:users,email',
            // the password should be sent twice, as "password" and "password_confirmation"
            'password' => 'required|string|confirmed'
        ]);

        $user = User::create([
            'name' => $fields['name'],
            'email' => $fields['email'],
            'password' => bcrypt($fields['password'])
        ]);

        // createToken is a method of HasApiToken, which User satisfies
        $token = $user->createToken('myapptoken')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 201);
    }


    public function login(Request $request)
    {
        $fields = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        // find the user who matches first the given email
        // emails are unique so this is lazy syntax
        $user = User::where('email', $fields['email'])->first();
        if (!$user) {
            return response([
                'message' => 'No user is registered with that email'
            ], 401);
        }

        if (
            // use bcrypt to...
            !Hash::check(
                // ...hash the provided password
                $fields['password'],
                // ...compare it with the stored hash
                $user->password
            )
        ) {
            return response([
                'message' => 'Wrong password'
            ], 401);
        }

        $token = $user->createToken('myapptoken')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 201);
    }

    public function logout(Request $request)
    {
        // get the auth instance
        // retrieve the authenticated user
        // find all her tokens
        // delete the tokens
        auth()->user()->tokens()->delete();

        return [
            'message' => "Logged out"
        ];
    }
}
