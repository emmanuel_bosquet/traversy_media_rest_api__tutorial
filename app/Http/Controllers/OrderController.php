<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    /**
     * Display a listing of all orders
     * This should be accessible only by an admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // find the user that sent the request
        // the path is protected anyway
        $user = auth()->user();
        Log::info('Received request from user', [$user]);

        $orders = $user->orders;
        Log::info('This user passed those orders', [$orders]);

        if (count($orders) == 0) {
            return response(["message" => "This user has passed no orders"], 204);
        } else {
            return $orders;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // find the user that sent the request
        // the path is protected anyway
        $user = auth()->user();
        Log::info('Received request from to store an order.', [
            'sent by user' => $user,
            'request' => $request,
        ]);

        // make sure the request contains a 'products' array and that it contains strings
        $fields = $request->validate([
            'products' => 'required|array',
            'products.*' => 'required|string'
        ]);

        Log::info(
            'Here are the request parameters once sanitized:',
            ['parameters' => $fields]
        );

        $products = $fields['products'];

        $totalPaymentDue = 0;

        Log::info("Checking the items in the database...");
        Log::info("There are that many products:", ['count' => count($products)]);

        for ($i = 0; $i < count($products); $i++) {

            Log::info("Checking for this product in the database:", ["product id" => $products[$i]]);
            $product = Product::find($products[$i]);

            Log::info("Found this product:", ['product' => $product]);

            // verify their existence
            if ($product == null) {
                return response("Product ${product[$i]} is gone apparently :-)", 410);
            }

            // if the product exist, add its price to the total
            $totalPaymentDue += $product->price;
        };

        Log::info("Total payment is:", ['totalAmountDue' => $totalPaymentDue]);

        $order = Order::create([
            'user_id' => $user->id,
            'total_payment_due' => $totalPaymentDue
        ]);

        $order->products()->attach($products);

        return $order;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        $order->products;

        return $order;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
