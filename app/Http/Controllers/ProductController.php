<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return Product::all();

        $products = Product::all();

        Log::info('Received request from to store an order.', [
            'sent by user' => $products,
            'request' => $products,
        ]);


        if (count($products) == 0) {
            return response([ "message" => "There are no products to display"], 200);
        } else {
            return $products;
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ->all() does… stuff
        $request->validate([
            'name' => 'required',
            'slug' => 'required',
            'price' => 'required'
        ]);

        return Product::create($request->all());

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Product::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->update($request->all());
        return $product;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // returns 1 if the item has been deleted
        // returns 0 if the item has not been deleted (non-existant, for instance)
        $successCode = Product::destroy($id);

        if ($successCode == 0) {

            $response = [
                'message' => "Item not deleted. Are you sure the id is correct?"

            ];
            return response($response, 400);
        }
        $response = [
            'message' => "item $id successfully deleted"
        ];

        return response($response, 200);
    }

    /**
     * Search for product that matches the name
     *
     * @param  string $name
     * @return \Illuminate\Http\Response
     */
    public function search($name)
    {
        return Product::where('name', 'like', '%' . $name . '%')->get();
    }
}
